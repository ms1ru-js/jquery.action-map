/**
 * jquery.action-map
 * @author Alexander Burtsev, http://burtsev.me, 2014
 * @license MIT
 */
(function($) {
	'use strict';

	var classPositive = 'plus',
		classNegative = 'minus',
		isSVG;

	// SVG detection
	try {
		isSVG = document.implementation.hasFeature('http://www.w3.org/TR/SVG11/feature#BasicStructure', '1.1');
	} catch(e) {}

	function createSVG(tag, attrs) {
		var el = document.createElementNS('http://www.w3.org/2000/svg', tag);
		for (var attr in attrs) {
			if ( attrs.hasOwnProperty(attr) ) {
				el.setAttribute(attr, attrs[attr]);
			}
		}
		return el;
	}

	function createVML(tag, attr) {
		var el = document.createElementNS('http://www.w3.org/2000/svg', tag, attr || {});
		return el;
	}

	$.fn.actionMap = function(opts, callback) {
		return $.each(this, function() {
			var	_this = $(this),
				_image = $('.action-map__image', this),
				_layer = $('.action-map__layer', this),
				_map = $('.action-map__areas', this),
				_areas = $('area', _map),
				_polygons,
				_canvas;

			var	canvasWidth = _image.width(),
				canvasHeight = _image.height();

			opts = $.extend({
				duration: 200, // ms
				stroke: '#FFF',
				fill: '#FFF',
				'stroke-width': '2px',
				'fill-opacity': 0.5
			}, opts || {});

			callback = callback || function() {};

			function createCanvas() {
				if ( isSVG ) {
					_canvas = $(createSVG('svg', {
						width: canvasWidth,
						height: canvasHeight,
						viewPort: '0 0 ' + canvasWidth + ' ' + canvasHeight
					})).appendTo(_layer);
				} else {
					_canvas = $(createVML('svg'))
						.css({
							width: canvasWidth + 'px',
							height: canvasHeight + 'px'
						})
						.appendTo(_layer);
				}
			}

			function createPolygon(points) {
				var _poly, poly;

				if ( isSVG ) {
					_poly = $(createSVG('polygon', $.extend({ points: points }, opts)))
						.appendTo(_canvas)
						.css('opacity', 0);
				} else {
					poly = createVML('polygon', {
						width: canvasWidth,
						height: canvasHeight
					});

					poly.setAttribute('points', points);
					poly.setAttribute('stroke', opts.stroke);
					poly.setAttribute('stroke-width', opts['stroke-width']);
					poly.setAttribute('fill', opts.fill);
					poly.setAttribute('fill-opacity', 0.5);

					_poly = $(poly).appendTo(_canvas);
					_poly.css('display', 'none');
				}

				return _poly;
			}
		
			function setLabelValue(_el, value) {
				_el
					.removeClass(classPositive)
					.removeClass(classNegative)
					.addClass(value ? (value > 0 ? classPositive : classNegative) : '')
					.find('.js-value')
					.html(value || '');
			}

			// Sets canvas layer size
			_layer.css({
				width: canvasWidth,
				height: canvasHeight
			});

			// Creates canvas
			createCanvas();

			// Adds polygons for all areas
			_areas.each(function() {
				var _area = $(this),
					_poly = createPolygon(_area.attr('coords')),
					rel = _area.attr('rel'),
					isShape = _poly.is('shape'),
					fixed = false;

				if ( isShape ) {
					_poly.get(0).rel = rel;
				} else {
					_poly.attr('rel', rel);
				}

				_area
					.on('mouseenter', function() {
						if ( isShape ) {
							_polygons.each(function() {
								if ( this.rel === rel ) {
									this.style.display = '';
								}
							});
						} else {
							_polygons
								.filter('[rel="' + rel + '"]')
								.stop()
								.animate({ opacity: 1 }, opts.duration);
						}
					})
					.on('mouseleave', function() {
						if ( isShape ) {
							_polygons.each(function() {
								if ( this.rel === rel ) {
									this.style.display = 'none';
								}
							});
						} else {
							_polygons
								.filter('[rel="' + rel + '"]')
								.stop()
								.animate({ opacity: 0 }, opts.duration);
						}
					});
			});

			// Gets polygons
			if ( isSVG ) {
				_polygons = $('polygon', _canvas);
			} else {
				_polygons = $('shape', _canvas);
			}

			// Callback invocation
			callback.call(this, _polygons);
		});
	};

	$(function() {
		$('.action-map').each(function() {
			var _this = $(this),
				data = _this.data();

			if ( data.autoinit !== 'off' ) {
				_this.actionMap();
			}
		});
	});
})(jQuery);

/**
 * SVG to VML patch, partial (only polygons) SVG support for IE6-8
 */
(function() {
	if ( document.implementation.hasFeature('http://www.w3.org/TR/SVG11/feature#BasicStructure', '1.1')	) {
		return;
	}

	document.namespaces.add('v', 'urn:schemas-microsoft-com:vml');

	var	styleSheet = document.createStyleSheet(),
		vmlSelectors = [' *', 'roundrect', 'oval', 'fill', 'line', 'shape', 'polyline', 'stroke'],
		vmlValue = 'behavior:url(#default#VML); position:absolute';

	for (var i = 0; i < vmlSelectors.length; i++) {
		styleSheet.addRule('v\\:' + vmlSelectors[i], vmlValue);
	}

	var	attributes = {};

		/** @ignore */
		attributes.stroke = function(element, value) {
			element.stroked = value == 'none' ? 0 : 1;
			element.strokecolor = value;
		};
		
		/** @ignore */
		attributes.fill = function(element, value) {
			element.filled = value == 'none' ? 0 : 1;
			element.fillcolor = value ;
		};

		/** @ignore */
		attributes['stroke-width'] = function(element, value) {
			element.strokeweight = parseFloat(value) / 1.2 + 'pt';
		};
		
		/** @ignore */
		attributes['stroke-opacity'] = function(element, value) {
			var stroke = document.createElement('v:stroke');
			stroke.endcap = 'round';
			stroke.opacity = value;
			element.appendChild(stroke);
		};
		
		/** @ignore */
		attributes['fill-opacity'] = function(element, value) {
			var fill = document.createElement('v:fill');
			fill.opacity = value;
			element.appendChild(fill);
		};

		/** @ignore */
		attributes.d = function(element, value) {
			element.path = value;
		};

	document.createElementNS = function(ns, name, attr) {
		var	element, child,
			props = { x: 'left', y: 'top' },
			svg2vmlStyle = {
				fill: { value: '#000000', valueSource: 'default' },
				stroke: { value: 'none', valueSource: 'default' },
				'stroke-width': { value: '1', valueSource: 'default' }
			};

		switch (name) {
			case 'svg':
				element = document.createElement('div');
				element.setAttribute('baseInit', false);
				element.setAttribute('baseX', 0);
				element.setAttribute('baseY', 0);
			break;

			case 'polygon' :
				element = document.createElement('v:shape');
				element.endcap = 'square';
				element.coordsize = attr.width + ', ' + attr.height;
				element.style.width = attr.width + 'px';
				element.style.height = attr.height + 'px';

				element.svg2vmlStyle = svg2vmlStyle;

				/** @ignore */
				element.setAttribute = function(key, value, cascade) {
					if (this.svg2vmlStyle[key]){
						this.svg2vmlStyle[key].value = value;
						this.svg2vmlStyle[key].valueSource = (cascade || 'user');
					}

					if ( key == 'points' ) {
						var source = value.split(','),
							points = '';
						
						points += 'm' + source.shift();
						points += ',' + source.shift();
						points += 'l' + source.join(',');

						this.path = points;
					} else if ( attributes[key] ) {
						attributes[key](this, value);
					}
				};

				return element;
		}

		if ( name != 'svg' ) {
			for (var key in element.svg2vmlStyle) {
				if (
					element.svg2vmlStyle[key].value &&
					element.svg2vmlStyle[key].valueSource == 'default'
				) {
					element.setAttribute(key, element.svg2vmlStyle[key].value, 'default');
				}
			}
		}

		return element;
	};
})();
