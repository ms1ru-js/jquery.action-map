module.exports = function(grunt) {
	'use strict';

	require('matchdep')
		.filterDev('grunt-*')
		.forEach(grunt.loadNpmTasks);

	grunt.initConfig({
		jsSource: 'src/*.js',

		banner: 
			'/**\n' +
			' * jquery.action-map\n' +
			' * @author Alexander Burtsev, http://burtsev.me, <%= grunt.template.today("yyyy") %>\n' +
			' * @license MIT\n' +
			' */\n',

		jshint: {
			options: {
				globals: {
					jQuery: true
				}
			},
			source: ['<%= jsSource %>']
		},

		jscs: {
			options: {
				config: '.jscs.json'
			},
			source: ['<%= jsSource %>']
		},

		concat: {
			options: {
				banner: '<%= banner %>'
			},
			actionmap: {
				src: ['<%= jsSource %>'],
				dest: 'jquery.action-map.js'
			}
		},

		uglify: {
			options: {
				banner: '<%= banner %>'
			},
			actionmap: {
				files: {
					'jquery.action-map.min.js': ['<%= jsSource %>']
				}
			}
		},

		watch: {
			actionmap: {
				files: ['<%= jsSource %>'],
				tasks: ['jshint', 'jscs', 'concat', 'uglify']
			}
		}
	});

	grunt.registerTask('default', ['jshint', 'jscs', 'concat', 'uglify', 'watch']);
};
